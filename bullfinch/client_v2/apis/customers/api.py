from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import (
    customer_create,
    customer_find,
    customer_get,
    customer_update,
    customers_find,
)


class CustomersApi(BaseApi):
    customer_find = customer_find.make_request
    customers_find = customers_find.make_request
    customer_create = customer_create.make_request
    customer_get = customer_get.make_request
    customer_update = customer_update.make_request
