from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import comment_create, comments_list


class ContractCommentsApi(BaseApi):
    comments_list = comments_list.make_request
    comment_create = comment_create.make_request
