from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import (
    document_download,
    document_get,
    document_upload,
    documents_list,
    generate_documents_to_sign,
    get_customer_embedded_signing_url,
    start_embedded_signature_process,
    start_signature_process,
)


class ContractDocumentsApi(BaseApi):
    documents_list = documents_list.make_request
    document_upload = document_upload.make_request
    document_get = document_get.make_request
    document_download = document_download.make_request
    generate_documents_to_sign = generate_documents_to_sign.make_request
    start_signature_process = start_signature_process.make_request
    start_embedded_signature_process = start_embedded_signature_process.make_request
    get_customer_embedded_signing_url = get_customer_embedded_signing_url.make_request
