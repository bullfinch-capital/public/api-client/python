from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class EmbeddedCustomerSignReq(BaseModel):
    return_url: str


class CustomerEmbeddedSigningUrlRes(BaseModel):
    signing_url: str


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    __request__: EmbeddedCustomerSignReq,
    contract_id: str,
    customer_id: str,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> CustomerEmbeddedSigningUrlRes:
    """Get Customer Embedded Signing Url"""

    body = __request__

    m = ApiRequest(
        method="POST",
        path="/rest/v2/contracts/{contract_id}/documents/embedded_sign/customers/{customer_id}/url".format(
            contract_id=contract_id,
            customer_id=customer_id,
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": CustomerEmbeddedSigningUrlRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
