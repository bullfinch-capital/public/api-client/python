from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class FindInstallerReq(BaseModel):
    cleantech_id: typing.Optional[str] = None
    external_id: str


class Coordinates(BaseModel):
    latitude: typing.Optional[float] = None
    longitude: typing.Optional[float] = None


class Address(BaseModel):
    city: typing.Optional[str] = None
    country: typing.Optional[str] = None
    line1: typing.Optional[str] = None
    line2: typing.Optional[str] = None
    location: typing.Optional[Coordinates] = None
    postal_code: typing.Optional[str] = None


class Company(BaseModel):
    address: typing.Optional[Address] = None
    iban: typing.Optional[str] = None
    name: str


class InstallerData(BaseModel):
    cleantech_id: str
    company: Company
    external_id: str
    name: str


class InstallerRes(BaseModel):
    data: InstallerData
    installer_id: typing.Optional[str] = None


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    __request__: FindInstallerReq,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> InstallerRes:
    """Find Installer"""

    body = __request__

    m = ApiRequest(
        method="POST",
        path="/rest/v2/installers/find_installer".format(),
        content_type="application/json",
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": InstallerRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "404": {
                "default": None,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
