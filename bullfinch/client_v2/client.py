from dataclasses import dataclass

from swagger_codegen.api.adapter.base import HttpClientAdapter
from swagger_codegen.api.client import ApiClient
from swagger_codegen.api.configuration import Configuration

from .apis.contract_comments.api import ContractCommentsApi
from .apis.contract_documents.api import ContractDocumentsApi
from .apis.contract_hardware.api import ContractHardwareApi
from .apis.contracts.api import ContractsApi
from .apis.customers.api import CustomersApi
from .apis.installers.api import InstallersApi
from .apis.price.api import PriceApi


@dataclass
class AutogeneratedApiClient:
    configuration: Configuration
    client: ApiClient
    customers: CustomersApi
    contracts: ContractsApi
    contract_comments: ContractCommentsApi
    contract_documents: ContractDocumentsApi
    contract_hardware: ContractHardwareApi
    installers: InstallersApi
    price: PriceApi


def new_client(
    adapter: HttpClientAdapter, configuration: Configuration
) -> AutogeneratedApiClient:
    client = ApiClient(configuration=configuration, adapter=adapter)
    return AutogeneratedApiClient(
        configuration,
        client=client,
        customers=CustomersApi(client, configuration),
        contracts=ContractsApi(client, configuration),
        contract_comments=ContractCommentsApi(client, configuration),
        contract_documents=ContractDocumentsApi(client, configuration),
        contract_hardware=ContractHardwareApi(client, configuration),
        installers=InstallersApi(client, configuration),
        price=PriceApi(client, configuration),
    )
