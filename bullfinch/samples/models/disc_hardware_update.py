import typing

import pydantic

from bullfinch.client_v2.apis.contract_hardware import hardware_update
from bullfinch.client_v2.apis.contract_hardware.hardware_update import (
    Timeline as Timeline,
)


class HardwareDetailsBattery(hardware_update.HardwareDetailsBattery):
    type: typing.Literal["batteryStorage"]


class HardwareDetailsEnergyManagementSystem(
    hardware_update.HardwareDetailsEnergyManagementSystem
):
    type: typing.Literal["energyManagementSystem"]


class HardwareDetailsEnergyMeter(hardware_update.HardwareDetailsEnergyMeter):
    type: typing.Literal["energyMeter"]


class HardwareDetailsEvCharger(hardware_update.HardwareDetailsEvCharger):
    type: typing.Literal["EVCharger"]


class HardwareDetailsHeatPump(hardware_update.HardwareDetailsHeatPump):
    type: typing.Literal["heatPump"]


class HardwareDetailsInverter(hardware_update.HardwareDetailsInverter):
    type: typing.Literal["inverter"]


class HardwareDetailsMeterCabinet(hardware_update.HardwareDetailsMeterCabinet):
    type: typing.Literal["meterCabinet"]


class HardwareDetailsMountingSystem(hardware_update.HardwareDetailsMountingSystem):
    type: typing.Literal["mountingSystem"]


class HardwareDetailsOptimizers(hardware_update.HardwareDetailsOptimizers):
    type: typing.Literal["optimizers"]


class HardwareDetailsPV(hardware_update.HardwareDetailsPV):
    type: typing.Literal["solarPV"]


class HardwareDetailsSpecialScaffolding(
    hardware_update.HardwareDetailsSpecialScaffolding
):
    type: typing.Literal["specialScaffolding"]


class HardwarePatchReq(hardware_update.HardwarePatchReq):
    details: typing.Optional[
        typing.Union[
            HardwareDetailsBattery,
            HardwareDetailsEnergyManagementSystem,
            HardwareDetailsEnergyMeter,
            HardwareDetailsEvCharger,
            HardwareDetailsHeatPump,
            HardwareDetailsInverter,
            HardwareDetailsMeterCabinet,
            HardwareDetailsMountingSystem,
            HardwareDetailsOptimizers,
            HardwareDetailsPV,
            HardwareDetailsSpecialScaffolding,
        ]
    ] = pydantic.Field(discriminator="type")
