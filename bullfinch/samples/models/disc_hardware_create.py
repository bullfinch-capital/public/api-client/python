import typing

import pydantic

from bullfinch.client_v2.apis.contract_hardware import hardware_create


class HardwareDetailsBattery(hardware_create.HardwareDetailsBattery):
    type: typing.Literal["batteryStorage"]


class HardwareDetailsEnergyManagementSystem(
    hardware_create.HardwareDetailsEnergyManagementSystem
):
    type: typing.Literal["energyManagementSystem"]


class HardwareDetailsEnergyMeter(hardware_create.HardwareDetailsEnergyMeter):
    type: typing.Literal["energyMeter"]


class HardwareDetailsEvCharger(hardware_create.HardwareDetailsEvCharger):
    type: typing.Literal["EVCharger"]


class HardwareDetailsHeatPump(hardware_create.HardwareDetailsHeatPump):
    type: typing.Literal["heatPump"]


class HardwareDetailsInverter(hardware_create.HardwareDetailsInverter):
    type: typing.Literal["inverter"]


class HardwareDetailsMeterCabinet(hardware_create.HardwareDetailsMeterCabinet):
    type: typing.Literal["meterCabinet"]


class HardwareDetailsMountingSystem(hardware_create.HardwareDetailsMountingSystem):
    type: typing.Literal["mountingSystem"]


class HardwareDetailsOptimizers(hardware_create.HardwareDetailsOptimizers):
    type: typing.Literal["optimizers"]


class HardwareDetailsPV(hardware_create.HardwareDetailsPV):
    type: typing.Literal["solarPV"]


class HardwareDetailsSpecialScaffolding(
    hardware_create.HardwareDetailsSpecialScaffolding
):
    type: typing.Literal["specialScaffolding"]


class HardwarePostReq(hardware_create.HardwarePostReq):
    details: typing.Optional[
        typing.Union[
            HardwareDetailsBattery,
            HardwareDetailsEnergyManagementSystem,
            HardwareDetailsEnergyMeter,
            HardwareDetailsEvCharger,
            HardwareDetailsHeatPump,
            HardwareDetailsInverter,
            HardwareDetailsMeterCabinet,
            HardwareDetailsMountingSystem,
            HardwareDetailsOptimizers,
            HardwareDetailsPV,
            HardwareDetailsSpecialScaffolding,
        ]
    ] = pydantic.Field(discriminator="type")


pass
