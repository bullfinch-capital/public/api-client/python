import typing

import pydantic

from bullfinch.client_v2.apis.contract_hardware import hardware_bulk_create
from bullfinch.samples.models.disc_hardware_create import HardwarePostReq


class HardwareDetailsBattery(hardware_bulk_create.HardwareDetailsBattery):
    type: typing.Literal["batteryStorage"]


class HardwareDetailsEnergyManagementSystem(
    hardware_bulk_create.HardwareDetailsEnergyManagementSystem
):
    type: typing.Literal["energyManagementSystem"]


class HardwareDetailsEnergyMeter(hardware_bulk_create.HardwareDetailsEnergyMeter):
    type: typing.Literal["energyMeter"]


class HardwareDetailsEvCharger(hardware_bulk_create.HardwareDetailsEvCharger):
    type: typing.Literal["EVCharger"]


class HardwareDetailsHeatPump(hardware_bulk_create.HardwareDetailsHeatPump):
    type: typing.Literal["heatPump"]


class HardwareDetailsInverter(hardware_bulk_create.HardwareDetailsInverter):
    type: typing.Literal["inverter"]


class HardwareDetailsMeterCabinet(hardware_bulk_create.HardwareDetailsMeterCabinet):
    type: typing.Literal["meterCabinet"]


class HardwareDetailsMountingSystem(hardware_bulk_create.HardwareDetailsMountingSystem):
    type: typing.Literal["mountingSystem"]


class HardwareDetailsOptimizers(hardware_bulk_create.HardwareDetailsOptimizers):
    type: typing.Literal["optimizers"]


class HardwareDetailsPV(hardware_bulk_create.HardwareDetailsPV):
    type: typing.Literal["solarPV"]


class HardwareDetailsSpecialScaffolding(
    hardware_bulk_create.HardwareDetailsSpecialScaffolding
):
    type: typing.Literal["specialScaffolding"]


class HardwareBulkPostReq(hardware_bulk_create.HardwareBulkPostReq):
    hardware: typing.List[HardwarePostReq]  # type: ignore
