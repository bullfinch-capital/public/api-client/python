import datetime
from dataclasses import dataclass
from enum import Enum
from typing import Collection, Optional, TypeVar

import faker

import bullfinch.client_v2.apis.customers.customer_create as customer_create
from bullfinch.client_v2.apis.contract_hardware import hardware_update
from bullfinch.client_v2.apis.contracts import contract_update


class Country(str, Enum):
    de = "DE"
    es = "ES"
    it = "IT"


class Product(str, Enum):
    # Remote Flow
    rent = "rent"
    bnpl = "bnpl"
    loan = "loan"
    mietkauf = "mietkauf"

    # Door to Door Flow
    bnpl_d2d = "bnpl_d2d"
    mietkauf_d2d = "mietkauf_d2d"


def country2locale(c: Country) -> str:
    return {
        Country.de: "de_DE",
        Country.es: "es_ES",
        Country.it: "it_IT",
    }.get(c, "de_DE")


def country2international_prefix(c: Country) -> str:
    return {
        Country.de: "+49",
        Country.es: "+34",
        Country.it: "+39",
    }.get(c, "+49")


T = TypeVar("T")


class DataFactory:
    def __init__(self, country: Country, product: Product):
        self.country = country
        self.product = product
        self.f = faker.Faker(country2locale(self.country))
        self._pv_price: Optional[int] = None
        self._battery_price: Optional[int] = None
        self._ev_charger_price: Optional[int] = None

    @property
    def is_cl_es(self) -> bool:
        return self.product == Product.loan and self.country == Country.es

    def random_choice(self, choices: Collection[T]) -> T:
        return self.f.random_choices(choices, 1)[0]

    def numerify(self, *args, **kwargs):
        return self.f.numerify(*args, **kwargs)

    def image(self, *args, **kwargs):
        return self.f.image(*args, **kwargs)

    def date_of_birth(self):
        return self.f.date_of_birth(minimum_age=25, maximum_age=59)

    def international_prefix(self):
        return country2international_prefix(self.country)

    def phone_number(self):
        return self.f.numerify("#### ### ###")

    def amount(self, min_value: Optional[int], max_value: Optional[int]) -> int:
        return self.f.pyint(min_value=min_value or 0, max_value=max_value or 20000)

    def net_price(self, min_euros_value: int, max_euros_value: int):
        return hardware_update.Money(
            value_cents=self.amount(min_euros_value, max_euros_value) * 100,
            currency="eur",
        )

    def gross_price(self, net_price, vat_rate: float):
        assert vat_rate < 1, "vat rate should look like .19 for a 19% rate"
        return hardware_update.Money(
            value_cents=int(net_price.value_cents * (1 + vat_rate)),
            currency=net_price.currency,
        )

    def pv_price_euros(self):
        if self._pv_price is None:
            self._pv_price = self.amount(5000, 10000)
        return self._pv_price

    def pv_gross_price(self):
        if self.country != Country.de:
            return hardware_update.Money(
                value_cents=self.pv_price_euros() * 100,
                currency="eur",
            )
        return None

    def pv_net_price(self):
        if self.country == Country.de:
            return hardware_update.Money(
                value_cents=self.pv_price_euros() * 100,
                currency="eur",
            )
        return None

    def pv_production_capacity(self) -> float:
        return 14.0

    def battery_price_euros(self):
        if self._battery_price is None:
            self._battery_price = self.amount(2000, 4500)
        return self._battery_price

    def battery_gross_price(self):
        if self.country != Country.de:
            return hardware_update.Money(
                value_cents=self.battery_price_euros() * 100,
                currency="eur",
            )
        return None

    def battery_net_price(self):
        if self.country == Country.de:
            return hardware_update.Money(
                value_cents=self.battery_price_euros() * 100,
                currency="eur",
            )
        return None

    def battery_storage_capacity(self) -> float:
        return 5.5

    def ev_charger_price_euros(self):
        if self._ev_charger_price is None:
            self._ev_charger_price = self.amount(500, 1000)
        return self._ev_charger_price

    def ev_charger_gross_price(self):
        if self.country != Country.de:
            return hardware_update.Money(
                value_cents=self.ev_charger_price_euros() * 100,
                currency="eur",
            )
        return None

    def ev_charger_net_price(self):
        if self.country == Country.de:
            return hardware_update.Money(
                value_cents=self.ev_charger_price_euros() * 100,
                currency="eur",
            )
        return None

    def ev_charger_power(self) -> float:
        return 9

    def optimizers_power_kw(self) -> float:
        return 4.5

    def downpayment(self):
        amount = self.pv_price_euros() + self.battery_price_euros()
        percentage = 5
        return contract_update.Money(
            value_cents=amount * 100 * percentage // 100, currency="eur"
        )

    @dataclass
    class Person:
        first_name: str
        last_name: str
        gender: str
        email: str
        date_of_birth: datetime.date
        tax_id: Optional[str]

    def person(self):
        p = self.f.profile()
        first_name, last_name = p["name"].rsplit(" ", 1)
        gender = {
            "M": "male",
            "F": "female",
        }.get(p["sex"].upper(), "unspecified")
        if self.country == "ES":
            tax_id = self.f.numerify("%#######") + self.f.lexify("?").upper()
        else:
            tax_id = None
        return DataFactory.Person(
            first_name=first_name,
            last_name=last_name,
            gender=gender,
            email="fake-" + p["mail"],
            date_of_birth=self.date_of_birth(),
            tax_id=tax_id,
        )

    def address(self):
        p = self.f.profile()
        line1, line2 = p["address"].rsplit("\n", 1)
        postal_code, city = line2.split(" ", 1)
        return customer_create.ValidAddress(
            line1=line1,
            line2=line2,
            city=city,
            postal_code=postal_code,
            country=self.country,
        )
