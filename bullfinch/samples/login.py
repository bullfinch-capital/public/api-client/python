"""The simplest possible user journey: login and retrieve something."""

import logging

import requests
from swagger_codegen.api.adapter.requests import RequestsAdapter
from swagger_codegen.api.configuration import Configuration

import bullfinch

from . import define_arguments


def main():
    args = define_arguments()
    if args.quiet:
        logging.getLogger().setLevel(logging.ERROR)
    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    bearer_token = bullfinch.login(args.client_id, args.secret, args.auth_endpoint)

    configuration = Configuration(host=args.api_endpoint)
    session = requests.Session()
    session.headers["Authorization"] = "Bearer " + bearer_token
    client = bullfinch.new_client(
        adapter=RequestsAdapter(debug=args.verbose, session=session),
        configuration=configuration,
    )
    assert client

    if args.quiet:
        print(bearer_token)
        return

    logging.info("Bearer %s", bearer_token)


if __name__ == "__main__":
    main()
